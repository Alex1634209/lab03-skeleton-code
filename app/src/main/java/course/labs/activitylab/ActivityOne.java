package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		// lifecycle counts
		//TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.

		//TODO:  increment the variables' values when their corresponding lifecycle methods get called.
	    int onCreateCounter=0; int onStartCounter=0; int onResumeCounter=0; int onPauseCounter=0;
	    int onStopCounter=0; int onRestartCounter=0; int onDestroyCounter=0;
	    TextView create,start,resume,pause,stop,restart,destroy;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_one);


			create = (TextView)findViewById(R.id.create);
			start = (TextView)findViewById(R.id.start);
			resume = (TextView)findViewById(R.id.resume);
			pause = (TextView)findViewById(R.id.pause);
			stop = (TextView)findViewById(R.id.stop);
			restart = (TextView)findViewById(R.id.restart);
			destroy = (TextView)findViewById(R.id.destroy);
			//Log cat print out
			Log.i(TAG, "onCreate A called");

			onCreateCounter++;
			create.setText(String.valueOf(onCreateCounter));
			//TODO: update the appropriate count variable & update the view
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}
		
		// lifecycle callback overrides
		
		@Override
		public void onStart(){
			super.onStart();

			//Log cat print out
			Log.i(TAG, "onStart A called");
			onStartCounter++;
			start.setText(String.valueOf(onStartCounter));
			//TODO:  update the appropriate count variable & update the view
		}

	    // TODO: implement 5 missing lifecycle callback methods


	    @Override
		public void onResume(){
			super.onResume();
			Log.i(TAG,"onResume A called");
			onRestartCounter++;
			resume.setText(String.valueOf(onResumeCounter));
		}

		@Override
		public void onPause(){
			super.onPause();
			Log.i(TAG,"onPause A called");
			onPauseCounter++;
			pause.setText(String.valueOf(onPauseCounter));
		}

		@Override
		public void onStop(){
			super.onStop();
			Log.i(TAG,"onStop A called");
			onStopCounter++;
			stop.setText(String.valueOf(onStopCounter));
		}

		@Override
		public void onRestart(){
			super.onRestart();
			Log.i(TAG,"onRestart A called");
			onRestartCounter++;
			restart.setText(String.valueOf(onRestartCounter));
		}

		@Override
		public void onDestroy(){
			super.onDestroy();
			Log.i(TAG,"onDestroy A called");
			onDestroyCounter++;
			destroy.setText(String.valueOf(onDestroyCounter));
		}
	    // Note:  if you want to use a resource as a string you must do the following
	    //  getResources().getString(R.string.stringname)   returns a String.

		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			//TODO:  save state information with a collection of key-value pairs & save all  count variables
		}


		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}
		

}
