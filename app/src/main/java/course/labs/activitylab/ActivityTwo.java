package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;

public class ActivityTwo extends Activity {

	// string for logcat documentation
	private final static String TAG = "Lab-ActivityTwo";
	int onCreateCounter=0; int onStartCounter=0; int onResumeCounter=0; int onPauseCounter=0;
	int onStopCounter=0; int onRestartCounter=0; int onDestroyCounter=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_two);
		//Log cat print out
		Log.i(TAG, "onCreate B called");
		onCreateCounter++;
	}

	@Override
	protected void onStart() {
		super.onStart();
		setContentView(R.layout.activity_two);
		//Log cat print out
		Log.i(TAG, "onStart B called");
		onStartCounter++;
	}

	@Override
	protected void onResume() {
		super.onResume();
		setContentView(R.layout.activity_two);
		//Log cat print out
		Log.i(TAG, "onResume B called");
		onResumeCounter++;
	}

	@Override
	protected void onPause() {
		super.onPause();
		setContentView(R.layout.activity_two);
		//Log cat print out
		Log.i(TAG, "onPause B called");
		onPauseCounter++;
	}

	@Override
	protected void onStop() {
		super.onStop();
		setContentView(R.layout.activity_two);
		//Log cat print out
		Log.i(TAG, "onStop B called");
		onStopCounter++;
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		setContentView(R.layout.activity_two);
		//Log cat print out
		Log.i(TAG, "onRestart B called");
		onRestartCounter++;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		setContentView(R.layout.activity_two);
		//Log cat print out
		Log.i(TAG, "onDestroy B called");
		onDestroyCounter++;
	}
}
